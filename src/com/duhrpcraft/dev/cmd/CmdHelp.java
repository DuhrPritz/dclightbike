package com.duhrpcraft.dev.cmd;

/**
 *
 * @author Daniel R. Pritzker
 */
import com.duhrpcraft.dev.DCLB;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdHelp
{
  DCLB plugin;
  
  public CmdHelp(DCLB plugin)
  {
    this.plugin = plugin;
  }
  
  public final String tron = ChatColor.WHITE + "[" + ChatColor.BLUE + "TRON" + ChatColor.WHITE + "]";
  
  public void help(CommandSender sender, Command cmd, String[] args)
  {
    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("help")))
    {
      Player player = (Player)sender;
      player.sendMessage(ChatColor.GRAY + "==" + this.tron + ChatColor.GRAY + "==");
      player.sendMessage(ChatColor.GRAY + "Welcome to the offical tron help guide!");
      player.sendMessage(ChatColor.GRAY + "==" + ChatColor.BLUE + "Commands" + ChatColor.GRAY + "==");
      player.sendMessage(ChatColor.GRAY + "/tron join - Joins a game of tron.");
      player.sendMessage(ChatColor.GRAY + "/tron leave - Leaves the game of tron.");
      player.sendMessage(ChatColor.GRAY + "/tron setbluespawn - sets the spawnpoint for the blue team on spawn. ");
      player.sendMessage(ChatColor.GRAY + "/tron setredspawn - sets the spawnpoint for the red team on spawn.");
      player.sendMessage(ChatColor.GRAY + "/tron setlobbyspawn - sets the spawnpoint for the lobby.");
      player.sendMessage(ChatColor.GRAY + "==" + ChatColor.BLUE + "How to Play" + ChatColor.GRAY + "==");
      player.sendMessage(ChatColor.GRAY + "In the game of tron, the goal is to run the enemy team into your path of light, or blocks in this case. When the enemy team runs into your path of light, they will die, the goal of the game is to destroy the other team by destorying them by running them into your path.");
      player.sendMessage(ChatColor.GRAY + "Need extra help? Go to the developer page! http://dev.bukkit.org/bukkit-plugins/light-cycle-battles/");
    }
  }
}

