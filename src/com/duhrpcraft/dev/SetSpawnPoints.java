package com.duhrpcraft.dev;

/**
 *
 * @author Daniel R. Pritzker
 */
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class SetSpawnPoints
{
  DCLB plugin;
  
  public SetSpawnPoints(DCLB plugin)
  {
    this.plugin = plugin;
  }
  
  public final String tron = ChatColor.WHITE + "[" + ChatColor.BLUE + "TRON" + ChatColor.WHITE + "]";
  
  public void setBlueSpawn(CommandSender sender, Command cmd, String[] args)
  {
    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("setbluespawn")))
    {
      Player player = (Player)sender;
      Location loc = player.getLocation();
      if (player.hasPermission("tron.admin"))
      {
        this.plugin.getConfig().set("blueSpawn.World", loc.getWorld().getName());
        this.plugin.getConfig().set("blueSpawn.X", Double.valueOf(loc.getX()));
        this.plugin.getConfig().set("blueSpawn.Y", Double.valueOf(loc.getY()));
        this.plugin.getConfig().set("blueSpawn.Z", Double.valueOf(loc.getZ()));
        this.plugin.getConfig().set("blueSpawn.Yaw", Float.valueOf(loc.getYaw()));
        this.plugin.getConfig().set("blueSpawn.Pitch", Float.valueOf(loc.getPitch()));
        this.plugin.saveConfig();
        player.sendMessage("You have set the blue spawn!");
      }
      else
      {
        player.sendMessage(ChatColor.RED + "You do not permission to set spawns!");
      }
    }
  }
  
  public void setRedSpawn(CommandSender sender, Command cmd, String[] args)
  {
    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("setredspawn")))
    {
      Player player = (Player)sender;
      Location loc = player.getLocation();
      if (player.hasPermission("tron.admin"))
      {
        this.plugin.getConfig().set("redSpawn.World", loc.getWorld().getName());
        this.plugin.getConfig().set("redSpawn.X", Double.valueOf(loc.getX()));
        this.plugin.getConfig().set("redSpawn.Y", Double.valueOf(loc.getY()));
        this.plugin.getConfig().set("redSpawn.Z", Double.valueOf(loc.getZ()));
        this.plugin.getConfig().set("redSpawn.Yaw", Float.valueOf(loc.getYaw()));
        this.plugin.getConfig().set("redSpawn.Pitch", Float.valueOf(loc.getPitch()));
        this.plugin.saveConfig();
        player.sendMessage("You have set the red spawn!");
      }
      else
      {
        player.sendMessage(ChatColor.RED + "You do not permission to set spawns!");
      }
    }
  }
  
  public void setLobbySpawn(CommandSender sender, Command cmd, String[] args)
  {
    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("setlobbyspawn")))
    {
      Player player = (Player)sender;
      Location loc = player.getLocation();
      if (player.hasPermission("tron.admin"))
      {
        this.plugin.getConfig().set("lobbySpawn.World", loc.getWorld().getName());
        this.plugin.getConfig().set("lobbySpawn.X", Double.valueOf(loc.getX()));
        this.plugin.getConfig().set("lobbySpawn.Y", Double.valueOf(loc.getY()));
        this.plugin.getConfig().set("lobbySpawn.Z", Double.valueOf(loc.getZ()));
        this.plugin.getConfig().set("lobbySpawn.Yaw", Float.valueOf(loc.getYaw()));
        this.plugin.getConfig().set("lobbySpawn.Pitch", Float.valueOf(loc.getPitch()));
        this.plugin.saveConfig();
        player.sendMessage("You have set the lobby spawn!");
      }
      else
      {
        player.sendMessage(ChatColor.RED + "You do not permission to set spawns!");
      }
    }
  }
}
