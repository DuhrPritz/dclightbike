package com.duhrpcraft.dev;

/**
 *
 * @author Daniel R. Pritzker
 */
import java.io.PrintStream;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class EnterGame
{
  public ArrayList<String> game2 = new ArrayList();
  public ArrayList<String> red = new ArrayList();
  public ArrayList<String> blue = new ArrayList();
  
  public void join(CommandSender sender, Command cmd, String[] args, DCLB core)
  {
    Player player = (Player)sender;
    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("join")))
    {
      if (!core.allowjoin) {
        player.sendMessage(ChatColor.RED + "Tron has already started!");
      }
      if (core.allowjoin) {
        if (!this.game2.contains(player.getName()))
        {
          this.game2.add(player.getName());
          player.sendMessage(ChatColor.BLUE + "You have been added to the game!");
          
          World lobbyWorld = core.getServer().getWorld(core.getConfig().getString("lobbySpawn.World"));
          double xLobby = core.getConfig().getDouble("lobbySpawn.X");
          double yLobby = core.getConfig().getDouble("lobbySpawn.Y");
          double zLobby = core.getConfig().getDouble("lobbySpawn.Z");
          float LobbyPitch = core.getConfig().getInt("lobbySpawn.Pitch");
          float LobbyYaw = core.getConfig().getInt("lobbySpawn.Yaw");
          
          Location loc3 = new Location(lobbyWorld, xLobby, yLobby, zLobby, LobbyPitch, LobbyYaw);
          
          player.teleport(loc3);
          int average = (int)Math.ceil(1.0D * Bukkit.getOnlinePlayers().length / 2.0D);
          if (this.blue.size() < average)
          {
            if ((!this.blue.contains(player.getName())) && (!this.red.contains(player.getName())))
            {
              this.blue.add(player.getName());
              player.sendMessage("You have been added to the blue team");
              System.out.println(this.blue.toString());
            }
          }
          else if ((this.red.size() < average) && 
            (!this.blue.contains(player.getName())) && (!this.red.contains(player.getName())))
          {
            this.red.add(player.getName());
            player.sendMessage("You have been added to the red team!");
          }
        }
        else
        {
          player.sendMessage("You are already apart of TRON!");
        }
      }
    }
  }
  
  public void leave(CommandSender sender, Command cmd, String[] args)
  {
    Player player = (Player)sender;
    if ((args.length == 1) && 
      (args[0].equalsIgnoreCase("leave"))) {
      if (this.game2.contains(player.getName()))
      {
        player.sendMessage(ChatColor.RED + "You have left a game of tron!");
        this.game2.remove(player.getName());
        this.blue.remove(player.getName());
        this.red.remove(player.getName());
      }
      else
      {
        player.sendMessage(ChatColor.RED + "You are not in a game of tron");
      }
    }
  }
}

