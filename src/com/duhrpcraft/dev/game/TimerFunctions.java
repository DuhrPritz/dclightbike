package com.duhrpcraft.dev.game;

/**
 *
 * @author Daniel R. Pritzker
 */
import java.util.ArrayList;
import com.duhrpcraft.dev.DCLB;
import com.duhrpcraft.dev.EnterGame;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;

public class TimerFunctions
{
  public final String tron = ChatColor.WHITE + "[" + ChatColor.BLUE + "TRON" + ChatColor.WHITE + "]";
  public final DCLB core;
  
  public TimerFunctions(DCLB core)
  {
    this.core = core;
  }
  
  public void preGame()
  {
    this.core.allowjoin = true;
    Bukkit.broadcastMessage(this.tron + ChatColor.BLUE + " is now starting! Do /tron join to join the game!");
    
    this.core.getServer().getScheduler().runTaskLaterAsynchronously(this.core, new Runnable()
    {
      public void run()
      {
        TimerFunctions.this.core.allowjoin = false;
        Bukkit.broadcastMessage(TimerFunctions.this.tron + ChatColor.BLUE + " has started!");
        
        TimerFunctions.this.game();
      }
    }, this.core.getConfig().getLong("Game_Begin_Time") * 20L);
  }
  
  public void game()
  {
    this.core.allowjoin = false;
    World world = this.core.getServer().getWorld(this.core.getConfig().getString("blueSpawn.World"));
    double xBlue = this.core.getConfig().getDouble("blueSpawn.X");
    double yBlue = this.core.getConfig().getDouble("blueSpawn.Y");
    double zBlue = this.core.getConfig().getDouble("blueSpawn.Z");
    float bluePitch = this.core.getConfig().getInt("blueSpawn.Pitch");
    float blueYaw = this.core.getConfig().getInt("blueSpawn.Yaw");
    
    World world2 = this.core.getServer().getWorld(this.core.getConfig().getString("redSpawn.World"));
    double xRed = this.core.getConfig().getDouble("redSpawn.X");
    double yRed = this.core.getConfig().getDouble("redSpawn.Y");
    double zRed = this.core.getConfig().getDouble("redSpawn.Z");
    float RedPitch = this.core.getConfig().getInt("redSpawn.Pitch");
    float RedYaw = this.core.getConfig().getInt("redSpawn.Yaw");
    
    Location loc = new Location(world, xBlue, yBlue, zBlue, blueYaw, bluePitch);
    for (String player : this.core.getEnterGame().blue) {
      Bukkit.getPlayer(player).teleport(loc);
    }
    Location loc2 = new Location(world2, xRed, yRed, zRed, RedYaw, RedPitch);
    for (String player : this.core.getEnterGame().red) {
      Bukkit.getPlayer(player).teleport(loc2);
    }
    this.core.getServer().getScheduler().runTaskLaterAsynchronously(this.core, new Runnable()
    {
      public void run()
      {
        World lobbyWorld = TimerFunctions.this.core.getServer().getWorld(TimerFunctions.this.core.getConfig().getString("lobbySpawn.World"));
        double xLobby = TimerFunctions.this.core.getConfig().getDouble("lobbySpawn.X");
        double yLobby = TimerFunctions.this.core.getConfig().getDouble("lobbySpawn.Y");
        double zLobby = TimerFunctions.this.core.getConfig().getDouble("lobbySpawn.Z");
        float LobbyPitch = TimerFunctions.this.core.getConfig().getInt("lobbySpawn.Pitch");
        float LobbyYaw = TimerFunctions.this.core.getConfig().getInt("lobbySpawn.Yaw");
        
        Location loc3 = new Location(lobbyWorld, xLobby, yLobby, zLobby, LobbyPitch, LobbyYaw);
        for (String player : TimerFunctions.this.core.getEnterGame().game2) {
          Bukkit.getServer().getPlayerExact(player).teleport(loc3);
        }
        TimerFunctions.this.core.getEnterGame().blue.clear();
        TimerFunctions.this.core.getEnterGame().red.clear();
        TimerFunctions.this.core.getEnterGame().game2.clear();
        
        Bukkit.broadcastMessage(TimerFunctions.this.tron + ChatColor.BLUE + " has now finished!");
        for (Location l : TimerFunctions.this.core.blockschanged)
        {
          l.getBlock().setData(DyeColor.BLACK.getData());
          
          l.getBlock().setType(Material.WOOL);
        }
        TimerFunctions.this.core.blockschanged.clear();
        TimerFunctions.this.preGame();
      }
    }, this.core.getConfig().getLong("Game_Time") * 20L);
  }
}

