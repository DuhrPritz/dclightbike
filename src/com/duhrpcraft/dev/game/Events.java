package com.duhrpcraft.dev.game;

/**
 *
 * @author Daniel R. Pritzker
 */
import java.io.PrintStream;
import java.util.ArrayList;
import com.duhrpcraft.dev.DCLB;
import com.duhrpcraft.dev.EnterGame;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class Events
  implements Listener
{
  public final DCLB core;
  
  public Events(DCLB core)
  {
    this.core = core;
  }
  
  @EventHandler
  public void onDeathEvent(PlayerDeathEvent event)
  {
    System.out.println("swag");
    String player = event.getEntity().getPlayer().getName();
    World lobbyWorld = this.core.getServer().getWorld(this.core.getConfig().getString("lobbySpawn.World"));
    double xLobby = this.core.getConfig().getDouble("lobbySpawn.X");
    double yLobby = this.core.getConfig().getDouble("lobbySpawn.Y");
    double zLobby = this.core.getConfig().getDouble("lobbySpawn.Z");
    float LobbyPitch = this.core.getConfig().getInt("lobbySpawn.Pitch");
    float LobbyYaw = this.core.getConfig().getInt("lobbySpawn.Yaw");
    
    Location loc3 = new Location(lobbyWorld, xLobby, yLobby, zLobby, LobbyPitch, LobbyYaw);
    if ((event.getEntity() instanceof Player))
    {
      if (this.core.getEnterGame().game2.contains(player)) {
        this.core.getEnterGame().game2.remove(player);
      }
      if (this.core.getEnterGame().red.contains(player)) {
        this.core.getEnterGame().red.remove(player);
      }
      if (this.core.getEnterGame().blue.contains(player)) {
        this.core.getEnterGame().blue.remove(player);
      }
      if (this.core.getEnterGame().red.size() == 0)
      {
        Bukkit.broadcastMessage(ChatColor.BLUE + "Blue has won this game of tron!");
        for (String players : this.core.getEnterGame().game2) {
          Bukkit.getPlayerExact(players).teleport(loc3);
        }
      }
      if (this.core.getEnterGame().blue.size() == 0)
      {
        Bukkit.broadcastMessage(ChatColor.BLUE + "Red has won this game of tron!");
        for (String players : this.core.getEnterGame().game2) {
          Bukkit.getPlayerExact(players).teleport(loc3);
        }
      }
    }
  }
}

