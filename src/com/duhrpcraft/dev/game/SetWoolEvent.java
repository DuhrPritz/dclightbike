package com.duhrpcraft.dev.game;

/**
 *
 * @author Daniel R. Pritzker
 */
import java.util.ArrayList;
import com.duhrpcraft.dev.DCLB;
import com.duhrpcraft.dev.EnterGame;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.material.Wool;
import org.bukkit.plugin.java.JavaPlugin;

public class SetWoolEvent
  extends JavaPlugin
  implements Listener
{
  public final DCLB core;
  
  public SetWoolEvent(DCLB core)
  {
    this.core = core;
  }
  
  @EventHandler
  public void setWoolEffectBlue(PlayerMoveEvent event)
  {
    Player player = event.getPlayer();
    
    Location loc = player.getLocation();
    if (this.core.getEnterGame().blue.contains(player.getName()))
    {
      loc.setY(loc.getY() - 1.0D);
      Block block = loc.getBlock();
      if (block.getType() == Material.WOOL) {
        if ((block.getState().getData() instanceof Wool))
        {
          DyeColor color = ((Wool)block.getState().getData()).getColor();
          if (color == DyeColor.BLACK)
          {
            block.setType(Material.DIAMOND_BLOCK);
            Location location = block.getLocation();
            if (!this.core.blockschanged.contains(location)) {
              this.core.blockschanged.add(location);
            }
          }
        }
      }
    }
  }
  
  @EventHandler
  public void setWoolEffectRed(PlayerMoveEvent event)
  {
    Player player = event.getPlayer();
    Location loc = player.getLocation();
    if (this.core.getEnterGame().red.contains(player.getName()))
    {
      loc.setY(loc.getY() - 1.0D);
      Block block = loc.getBlock();
      if (block.getType() == Material.WOOL) {
        if ((block.getState().getData() instanceof Wool))
        {
          DyeColor color = ((Wool)block.getState().getData()).getColor();
          if (color == DyeColor.BLACK)
          {
            block.setType(Material.REDSTONE_BLOCK);
            Location location = block.getLocation();
            this.core.blockschanged.add(location);
          }
        }
      }
    }
  }
  
  @EventHandler
  public void setDeathEvent(PlayerMoveEvent event)
  {
    World world = this.core.getServer().getWorld(this.core.getConfig().getString("blueSpawn.World"));
    double xBlue = this.core.getConfig().getDouble("lobbySpawn.X");
    double yBlue = this.core.getConfig().getDouble("lobbySpawn.Y");
    double zBlue = this.core.getConfig().getDouble("lobbySpawn.Z");
    float bluePitch = this.core.getConfig().getInt("lobbySpawn.Pitch");
    float blueYaw = this.core.getConfig().getInt("lobbySpawn.Yaw");
    
    Location lobby = new Location(world, xBlue, yBlue, zBlue, blueYaw, bluePitch);
    
    Player player = event.getPlayer();
    Location loc = player.getLocation();
    if (this.core.getEnterGame().blue.contains(player.getName()))
    {
      loc.setY(loc.getY() - 1.0D);
      Block block = loc.getBlock();
      if (block.getType() == Material.REDSTONE_BLOCK)
      {
        player.teleport(lobby);
        player.setHealth(0.0D);
        loc.getWorld().createExplosion(loc, 0.0F);
      }
    }
    if (this.core.getEnterGame().red.contains(player.getName()))
    {
      loc.setY(loc.getY() - 1.0D);
      Block block1 = loc.getBlock();
      if (block1.getType() == Material.DIAMOND_BLOCK)
      {
        player.teleport(lobby);
        player.setHealth(0.0D);
        loc.getWorld().createExplosion(loc, 0.0F);
      }
    }
  }
}

