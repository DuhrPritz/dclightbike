package com.duhrpcraft.dev;

/**
 *
 * @author Daniel R. Pritzker
 */
import java.util.ArrayList;
import com.duhrpcraft.dev.cmd.CmdHelp;
import com.duhrpcraft.dev.game.Events;
import com.duhrpcraft.dev.game.SetWoolEvent;
import com.duhrpcraft.dev.game.TimerFunctions;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.FileConfigurationOptions;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class DCLB
  extends JavaPlugin
  implements Listener
{
  public ArrayList<Location> blockschanged = new ArrayList();
  TimerFunctions timer = new TimerFunctions(this);
  public boolean allowjoin;
  private EnterGame enter = new EnterGame();
  private SetSpawnPoints spawnpoints = new SetSpawnPoints(this);
  private SetWoolEvent setwoolevent = new SetWoolEvent(this);
  private CmdHelp cmdhelp = new CmdHelp(this);
  
  public void onEnable()
  {
    saveDefaultConfig();
    getConfig().options().copyDefaults(true);
    getServer().getPluginManager().registerEvents(new Events(this), this);
    getServer().getPluginManager().registerEvents(new SetWoolEvent(this), this);
    this.timer.preGame();
  }
  
  public final String tron = ChatColor.WHITE + "[" + ChatColor.BLUE + "TRON" + ChatColor.WHITE + "]";
  
  public void onDisable() {}
  
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
  {
    if (((sender instanceof Player)) && 
      (cmd.getName().equalsIgnoreCase("tron")))
    {
      Player player = (Player)sender;
      if (args.length == 0) {
        player.sendMessage(this.tron + ChatColor.BLUE + "Not enough arguements! Do /tron help for help!");
      }
      this.enter.join(sender, cmd, args, this);
      this.enter.leave(sender, cmd, args);
      this.spawnpoints.setBlueSpawn(sender, cmd, args);
      this.spawnpoints.setRedSpawn(sender, cmd, args);
      this.spawnpoints.setLobbySpawn(sender, cmd, args);
      this.cmdhelp.help(sender, cmd, args);
      return true;
    }
    return true;
  }
  
  public EnterGame getEnterGame()
  {
    return this.enter;
  }
  
  public SetWoolEvent getSetWoolEvent()
  {
    return this.setwoolevent;
  }
}
